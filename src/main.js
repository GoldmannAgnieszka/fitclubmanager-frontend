import Vue from 'vue'
import App from './App.vue'
import store from './store.js'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import '@mdi/font/css/materialdesignicons.css'



// Global registration

import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vue-material'
import adminRouter from "./routers/adminRouter";
import clientRouter from "./routers/clientRouter";
import receptionistRouter from "./routers/receptionistRouter";
import trainerRouter from "./routers/trainerRouter";
import {APP_TYPE} from "./constants";
import VueSingleSelect from "vue-single-select";



Vue.component('vue-single-select', VueSingleSelect);
Vue.config.silent = true
Vue.use(VueMaterial)
Vue.use(Vuetify,{
  iconfont: 'mdi' // 'md' || 'mdi' || 'fa' || 'fa4'

})



Vue.config.productionTip = false

const host = window.location.host;
const parts = host.split(':');








if(APP_TYPE === 'ADMIN')
{
  new Vue({
    router: adminRouter,
    store,
    render: h => h(App)
  }).$mount('#app')
}
if(APP_TYPE === 'CLIENT')
{
  new Vue({
    router: clientRouter,
    store,
    render: h => h(App)
  }).$mount('#app')
}
if(APP_TYPE === 'RECEPTIONIST')
{
  new Vue({
    router: receptionistRouter,
    store,
    render: h => h(App)
  }).$mount('#app')
}
if(APP_TYPE === 'TRAINER')
{
  new Vue({
    router: trainerRouter,
    store,
    render: h => h(App)
  }).$mount('#app')
}

