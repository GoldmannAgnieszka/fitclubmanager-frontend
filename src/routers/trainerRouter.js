import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import(/* webpackChunkName: "about" */ '@/views/Home.vue')
        },
        {
            path: '/login',
            name: 'login',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/trainerMenu/Login.vue')
        },
        {
            path: '/cennik',
            name: 'cennik',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/Prices.vue')
        },
        {
            path: '/grafik',
            name: 'grafik',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/trainerMenu/Schedule.vue')
        },
        {
            path: '/regulamin',
            name: 'regulamin',
            component: () => import(/* webpackChunkName: "about" */ '@/views/Regulation.vue')
        },
        {
            path: '/edit',
            name: 'edit',
            component: () => import(/* webpackChunkName: "about" */ '@/views/trainerMenu/Edit.vue')
        },
        {
            path: '/indywidualne',
            name: 'indywidualne',
            component: () => import(/* webpackChunkName: "about" */ '@/views/trainerMenu/IndividualTraningsManager.vue')
        },
        {
            path: '/trainings',
            name: 'trainings',
            component: () => import(/* webpackChunkName: "about" */ '@/views/trainerMenu/Trainings.vue')
        },
        {
            path: '/oferta',
            name: 'oferta',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/Offer.vue')
        },
        {
            path: '/trainers',
            name: 'trainers',
            component: () => import(/* webpackChunkName: "about" */ '@/views/Trainers.vue')
        },
        {
            path: '/grafik/:id',
            name: 'grafiktrenera',
            props: true,
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/trainerMenu/Schedule.vue')
        },

    ]
})
