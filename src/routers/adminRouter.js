import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({

    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import(/* webpackChunkName: "about" */ '@/views/Home.vue')
        },
        {
            path: '/klienci',
            name: 'klienci',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/ClientsManager.vue')
        },
       {
           path: '/login',
            name: 'login',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/Login.vue')
       },
        {
            path: '/cennik',
            name: 'cennik',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/Prices.vue')
        },

        {
            path: '/trenerzy',
            name: 'trenerzy',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/TrainersManager.vue')
        },

        {
            path: '/pracownicy',
            name: 'pracownicy',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/RecepcionistsManager.vue')
        },

        {
            path: '/grafik',
            name: 'grafik',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/Schedule.vue')
        },
        {
            path: '/ofertagrupowa',
            name: 'ofertagrupowa',
            // route level code-splitting
            // this generates a eparate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/GroupOffersManager.vue')
        },
        {
            path: '/oferta',
            name: 'oferta',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/Offer.vue')
        },
        {
            path: '/ofertaindywidualna',
            name: 'ofertaindywidualna',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/IndividualOffersManager.vue')
        },
        {
            path: '/treningigrupowe',
            name: 'treningigrupowe',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/GroupTrainingsManager.vue')
        },
        {
            path: '/karnety',
            name: 'karnety',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/PriceListManager.vue')
        },
        {
            path: '/eventsmanager',
            name: 'eventsmanager',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/EventsManager.vue')
        },
        {
            path: '/regulamin',
            name: 'regulamin',
            component: () => import(/* webpackChunkName: "about" */ '@/views/Regulation.vue')
        },
        {
            path: '/trainers',
            name: 'trainers',
            component: () => import(/* webpackChunkName: "about" */ '@/views/Trainers.vue')
        },
        {
            path: '/grafik/:id',
            name: 'grafiktrenera',
            props: true,
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/Schedule.vue')
        },

        {
            path: '/saleManager/:id',
            name: 'sale',
            props: true,
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/SaleManager.vue'),
            children: [
                {
                    path: "saleHistory",
                    component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/SaleHistory.vue')
                },
                {
                    path: "supplyHistory",
                    component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/SaleSupplyHistory.vue')
                },
                {
                    path: "sale",
                    component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/Sale.vue')
                },
                {
                    path: "supply",
                    component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/SaleSupply.vue')
                }]
        },
        {
            path: '/csvGenerator',
            name: 'csvGenerator',
            component: () => import(/* webpackChunkName: "about" */ '@/views/adminTables/CsvGenerator.vue')
        }

    ]
})
