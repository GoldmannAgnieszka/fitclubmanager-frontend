
import store from '../store'
class AuthorizationManagerService {
    isLoggedIn()
    {
        return store.getters.isAuthorized
    }
}

export default new AuthorizationManagerService()
