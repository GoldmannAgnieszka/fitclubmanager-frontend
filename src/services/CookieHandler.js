
import {APP_TYPE} from "../constants";

class CookieHandler {
    setAdminCookie(authKey){
        document.cookie="access_token_ad="+authKey
    }
    setClientCookie(authKey){
        document.cookie="access_token_cl="+authKey
    }
    setReceptionistCookie(authKey){
        document.cookie="access_token_re="+authKey
    }
    setTrainerCookie(authKey){
        document.cookie="access_token_tr="+authKey
    }

     checkIfAdminIsAuthorized(){
        if(this.getAuthKeyValueFromCookies())
            return true;
        else
            return false;

     }
     checkIfClientIsAuthorized(){
         if(this.getAuthKeyValueFromCookies())
             return true;
         else
             return false;
     }
     checkIfReceptionistIsAuthorized(){
         if(this.getAuthKeyValueFromCookies())
             return true;
         else
             return false;
     }
     checkIfTrainerIsAuthorized(){
         if(this.getAuthKeyValueFromCookies())
             return true;
         else
             return false;
     }

     checkIfAnyIsAuthorized()
     {
         var isAuthorized = false;
         if(this.checkIfClientIsAuthorized())
             isAuthorized = true;
         else if(this.checkIfReceptionistIsAuthorized())
             isAuthorized = true;
         else if(this.checkIfAdminIsAuthorized())
             isAuthorized = true;
         else if(this.checkIfTrainerIsAuthorized())
             isAuthorized = true;
         return isAuthorized;
     }

     getAuthKeyValueFromCookies(){
        var cookies = document.cookie;
        var cookieArray = cookies.split(' ');
        for(var x=0;x<cookieArray.length;x++){
            var cookie = cookieArray[x].split('=');
            var cookieName = cookie[0];
            var cookieValue = cookie[1];
            if(cookieName === this.getCookieTypeForCurrentRuntime() && (cookieValue.length>20 ))
                return cookieValue;
        }
     }

     getCookieTypeForCurrentRuntime()
     {
        if(APP_TYPE==='ADMIN'){
            return 'access_token_ad';
        }
        else if(APP_TYPE==='RECEPTIONIST'){
            return 'access_token_re'
        }
        else if(APP_TYPE==='CLIENT'){
            return 'access_token_cl'
        }
        else if(APP_TYPE==='TRAINER'){
            return 'access_token_tr'
        }
     }

     clearAuthLogout(){
        this.setAdminCookie('');
        this.setClientCookie('');
        this.setReceptionistCookie('');
        this.setTrainerCookie('');
    }

}

export default new CookieHandler();
