# FitClubManager - Frontend

# General informations
FitClubManager is web application designated for sport facilities like e.g. a gym, fit club, etc.

Its purpose is to help owners with management of such objects and human resources.

Contact us for informations about additional projects for extra functionalities.

Project was created as engineering project by students of Adam Mickiewicz University in Poznań, Poland.

This repository contains source code of frontend clients written in Vue.js framework.

# Properties in file constants
Explanation about some of properties that needs to be replaced in project to successfully run application:

# Set backend url

BASE_URL=


# Set app type
Possible values: "CLIENT", "RECEPTIONIST", "TRAINER", "ADMIN"

APP_TYPE=

# Extra modules
System is ready to use our extra modules, but they are not public. For more information feel free to contact us. 
Set these values as false

SALE_MODULE =
CSV_MODULE =
PAYMENTS_MODULE =
True/false values

# Start project
For detailed explanation on how things work, checkout the guide: http://vuejs-templates.github.io/webpack/

# Backend
Source code of backend application can be found here:

    https://bitbucket.org/arkpow1/fitclubmanager-main/src/master/

# License
Copyright [2020] [Arkadiusz Adam Powęska]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

# Contact emails
arkpow1@st.amu.edu.pl

agngol3@st.amu.edu.pl

krystianlipiec@gmail.comw